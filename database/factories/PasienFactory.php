<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\Pasien;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Factories\Factory;

class PasienFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pasien::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->name,
            'nik' => $this->faker->postcode,
            'tempat_lahir' => $this->faker->address,
            'tgl_lahir' => Carbon::now()->format("Y-m-d"),
            'alamat' => $this->faker->address,
            'tgl_penerimaan' => Carbon::now()->format("Y-m-d"),
            'tgl_pemeriksaan' => Carbon::now()->format("Y-m-d"),
            'no_sampel' => $this->faker->unique()->postcode,
            'status' => Arr::random(['POSITIF', 'NEGATIF']),
        ];
    }
}
