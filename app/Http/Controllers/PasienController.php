<?php

namespace App\Http\Controllers;

use App\Models\Pasien;
use Illuminate\Http\Request;
use App\Imports\PasienImport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Pasien::query();
        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action  = '<a href="/pasien/cetak_pdf/'.$data->id.'" target="_blank" class="btn-edit btn btn-primary btn-sm">PRINT</i></a>';
                    $action  .='&nbsp';
                    $action  .= '<a href="/pasien/'. $data->id.'/edit" target="_blank" class="btn-edit btn btn-info btn-sm">EDIT</i></a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('pasien');
    }

    public function create()
    {
        return view('create');
    }


    public function pasien_detail(Request $request)
    {
        $pasien = Pasien::where('no_sampel', $request->no_sampel)
            ->where('nik', $request->nik)
        ->first();

        return view('pasien-detail', compact('pasien'));
    }


	public function import_excel(Request $request)
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('file_pasien',$nama_file);
		Excel::import(new PasienImport, public_path('/file_pasien/'.$nama_file));
		Session::flash('sukses','Data Siswa Berhasil Diimport!');

		// alihkan halaman kembali
		return redirect('/home');
	}

    public function cetak_pdf($id)
    {
    	$pasien = Pasien::where('id', $id)->first();
        $url = env('URL_APP').'/pasien/detail?no_sampel='.$pasien->no_sampel.'&nik='.$pasien->nik;
        $qrcode = base64_encode(QrCode::format('svg')->size(150)->errorCorrection('H')->generate($url));

    	$pdf = PDF::loadview('pasien_pdf', compact('qrcode', 'pasien'));
    	return $pdf->stream('pasien.pdf');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'nik' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'tgl_penerimaan' => 'required',
            'tgl_pemeriksaan' => 'required',
            'no_sampel' => 'required',
            'status' => 'required'
        ]);

        $input = $request->all();

        $post = Pasien::create($input);

        if ($post) {
            //redirect dengan pesan sukses
            return redirect()->route('pasien.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->back()->withInput()->withErrors('Add atleast one image in the post.');
        }
    }

    public function edit($id)
    {
        $result = Pasien::find($id);
        return view('edit', compact('result'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'nik' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'tgl_penerimaan' => 'required',
            'tgl_pemeriksaan' => 'required',
            'no_sampel' => 'required',
            'status' => 'required'
        ]);

        $input = $request->all();

        $update = Pasien::find($id)->update($input);

        if ($update) {
            //redirect dengan pesan sukses
            return redirect()->route('pasien.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->back()->withInput()->withErrors('Add atleast one image in the post.');
        }
    }
}
