<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $lama = 7;
        // DB::select(DB::raw('DELETE FROM pasiens WHERE DATEDIFF(CURDATE(), tgl_pemeriksaan) > :lama'), [
        //     'lama' => $lama
        // ]);
        return view('home');
    }
}
