<?php

namespace App\Imports;

use App\Models\Pasien;
use Maatwebsite\Excel\Concerns\ToModel;

class PasienImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function formatDateExcel($date)
    {
        if (gettype($date) === 'double') {
            $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);
            return $birthday->format('n-j-Y');
        }
        return $date;
    }

    public function model(array $row)
    {
        return new Pasien([
            'nama' => $row[1],
            'nik' => $row[2],
            'tempat_lahir' => $this->formatDateExcel($row[3]),
            'tgl_lahir' => $row[4],
            'alamat' => $row[5],
            'tgl_penerimaan' => $this->formatDateExcel($row[6]),
            'tgl_pemeriksaan' => $this->formatDateExcel($row[7]),
            'no_sampel' => $row[8],
            'status' => $row[9],
        ]);
    }
}
