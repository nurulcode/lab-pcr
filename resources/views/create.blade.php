@extends('layouts.app')
@section('content')
<a href="{{ route('pasien.index') }}" class="btn btn-success"> PASIEN LIST</a>
<hr>
<div class="row">
    <div class="col-md-9">
        <form method="post" action="{{ route('pasien.store') }}">
            @csrf
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="nama">nama</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama" placeholder="Kolom Isi" value="{{ old('nama') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="nik">nik</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="nik" placeholder="Kolom Isi" value={{ old('nik') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="tempat_lahir">tempat lahir</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Kolom Isi" value={{ old('tempat_lahir') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="tgl_lahir">tgl lahir</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" name="tgl_lahir" placeholder="Kolom Isi" value={{ old('tgl_lahir') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="alamat">alamat</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="alamat" placeholder="Kolom Isi" value={{ old('alamat') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="tgl_penerimaan">tgl penerimaan</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" name="tgl_penerimaan" placeholder="Kolom Isi" value={{ old('tgl_penerimaan') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="tgl_pemeriksaan">tgl pemeriksaan</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" name="tgl_pemeriksaan" placeholder="Kolom Isi" value={{ old('tgl_pemeriksaan') }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="no_sampel">no sampel</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="no_sampel" placeholder="Kolom Isi" value={{ old('no_sampel') }}>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="status">hasil</label>
                <div class="col-sm-8">
                    <select class="form-control" name="status">
                        <option value="NEGATIF">NEGATIF</option>
                        <option value="POSITIF">POSITIF</option>
                    </select>
                </div>
            </div>
            <br>

            <div class="form-group row p-5">
                <label class="col-sm-4 col-form-label text-uppercase text-right" for="status"></label>
                <div class="col-sm-8">
                    <button type='text' class="btn btn-block btn-primary">TAMBAH PASIEN</button>
                </div>
            </div>
        </form>
    </div>

    <div>
        <div class="col-md-3">
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            <div class="alert-danger" role="alert">
                {{ $error }}
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>
@stop
