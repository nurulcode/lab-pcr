@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-light">
                        <tbody>
                            <tr>
                                <td class="text-uppercase font-wight-bold">Nama</td>
                                <td>:</td>
                                <td>{{ $pasien->nama }}</td>
                            </tr>
                            <tr>
                                <td class="text-uppercase font-wight-bold">nik</td>
                                <td>:</td>
                                <td>{{ $pasien->nik }}</td>
                            </tr>
                            <tr>
                                <td class="text-uppercase font-wight-bold">tgl lahir</td>
                                <td>:</td>
                                <td>{{ $pasien->tgl_lahir }}</td>
                            </tr>
                            <tr>
                                <td class="text-uppercase font-wight-bold">tgl pemeriksaan</td>
                                <td>:</td>
                                <td>{{ $pasien->tgl_pemeriksaan }}</td>
                            </tr>
                            <tr>
                                <td class="text-uppercase font-wight-bold">no sampel</td>
                                <td>:</td>
                                <td>{{ $pasien->no_sampel }}</td>
                            </tr>
                            <tr>
                                <td class="text-uppercase font-wight-bold">status</td>
                                <td>:</td>
                                <td>{{ $pasien->status }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
