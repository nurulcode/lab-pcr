<!DOCTYPE html>
<html>

<head>
    <title>Rincian Biata Pelayanan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        * {
            font-size: 13px;
        }

        .page-break {
            page-break-after: always;
        }

        .text-center {
            text-align: center;
        }

        table.table-bordered {
            border: 1px solid black;
            margin-top: 20px;
        }

        table.table-bordered>thead>tr>th {
            border: 1px solid black;
        }

        table.table-bordered>tbody>tr>td {
            border: 1px solid black;
        }

        /*  */

        /* table#table-header {
            border: none;
            margin-top: 100px;
            margin-bottom: 100px;
        } */

        /* table#table-header>thead>tr>th {
            border: none;
            padding: 0px;
            margin: 0px;
        } */

        table#table-header>tbody>tr>td {
            border: none;
            padding: 2px;
            margin: 0px;
        }

        table#table-footer {
            border: none;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        table#table-footer>thead>tr>th {
            border: none;
            padding: 0px;
            margin: 0px;
        }

        table#table-footer>tbody>tr>td {
            border: none;
            padding: 0px;
            margin: 0px;
        }


        /* tr td {
            padding: 2px !important;
        } */

        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid black;
            margin: 1em 0;
            padding: 0;
        }

    </style>
    <div style="text-align:center; font-weight:bold; font-size:14px; padding:3px;">PEMERINTAH KABUPATEN MANOKWARI</div>
    <div style="text-align:center; font-weight:bold; font-size:14px; padding:3px;">BADAN LAYANAN UMUM DAERAH RUMAH SAKIT UMUM MANOKWARI</div>
    <hr>
    <div style="text-align:center; font-weight:bold; font-size:12px;">SURAT KETERANGAN PEMERIKSAAN SWAP PCR</div>
    <br>
    <div style="text-align:center; font-weight:bold; font-size:12px;">NOMOR : ................................ </div>
    <br>
    <table id="table-header" class="table" style="padding:0; margin:0;">
        <thead>
            <tr>
                <td>Nama Pasien</td>
                <td>: {{ $pasien->nama }}</td>
            </tr>

            <tr>
                <td>NIK</td>
                <td>: {{ $pasien->nik }}</td>
            </tr>

            <tr>
                <td>Tgl Lahir</td>
                <td>: {{ $pasien->tgl_lahir }}</td>
            </tr>

            <tr>
                <td>Tgl Pemeriksaan</td>
                <td>: {{ $pasien->tgl_pemeriksaan }}</td>
            </tr>

            <tr>
                <td>Tgl Penerimaan</td>
                <td>: {{ $pasien->tgl_penerimaan }}</td>
            </tr>

            <tr>
                <td>No Sample</td>
                <td>: {{ $pasien->no_sampel }}</td>
            </tr>
            <tr>
                <td>Hasil</td>
                <td>: {{ $pasien->status }}</td>
            </tr>
        </thead>
    </table>
    <br>
    <hr>
    <div style="text-align:center; font-weight:bold;font-size:12px; padding:5px;">
        <img src="data:image/png;base64, {!! $qrcode !!}">
    </div>
    <hr>
    <br>
    <br>
    <table id="table-footer" class="table text-center pt-2">
        <tr>
            <td class="text-white">Pasien Keluarga</td>
            <td>Plt. Direktur BLUD RSU Manokwari</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="text-white">................................</td>
            <td class="font-weight-bold">dr.Alwan Rimosan, Sp.B,FINACS</td>
        </tr>
        <tr>
            <td class="text-white">................................</td>
            <td>Pembina</td>
        </tr>
        <tr>
            <td class="text-white">................................</td>
            <td>NIP</td>
        </tr>
    </table>
</body>

</html>
