@extends('layouts.app')
@section('content')
<a href="{{ route('home') }}" class="btn btn-success"> HOME</a>
<a href="{{ route('pasien.create') }}" class="btn btn-primary"> TAMBAH PASIEN</a>
<hr>
<table class="table table-bordered pt-5" id="users-table">
    <thead>
        <tr>
            <th>Nama</th>
            <th>NIK</th>
            <th>Tampat Lahir</th>
            <th>Tgl Lahir</th>
            <th>Tgl Periksa</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@stop

@push('scripts')
<script>
    $(function () {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/pasien',
            columns: [{
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'nik',
                    name: 'nik'
                },
                {
                    data: 'tempat_lahir',
                    name: 'tempat_lahir'
                },
                {
                    data: 'tgl_lahir',
                    name: 'tgl_lahir'
                },
                {
                    data: 'tgl_pemeriksaan',
                    name: 'tgl_pemeriksaan'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }
            ]
        });
    });

</script>
@endpush
